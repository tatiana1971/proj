package com.example.alert;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Timer;
import java.util.TimerTask;

public class dialog3 extends DialogFragment{
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstaceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("сейчас будет таймер")
                .setMessage("Диалоговое окно закроется через 5 секунд");
                AlertDialog dlg = builder.create();

        final  Timer timer = new Timer();
        timer.schedule(new TimerTask(){
            public void run(){
                dlg.dismiss();
                timer.cancel();
            }
        }, 3000);
        return dlg;
    }
}
