package com.example.alert;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showAlertDialog(View v){
        FragmentManager manager = getSupportFragmentManager();
        MyDialogFragment myDialogFragment = new MyDialogFragment();
        myDialogFragment.show(manager, "myDialog");
    }

    public void dialog2(View v){
        FragmentManager manager2 = getSupportFragmentManager();
        dialog2 dialog2 = new dialog2();
        dialog2.show(manager2, "myDialog2");
    }

    public void dialog3(View v){
        FragmentManager manager3 = getSupportFragmentManager();
        dialog3 dialog3 = new dialog3();
        dialog3.show(manager3, "myDialog3");
    }
}